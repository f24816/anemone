use std::fs;
use std::env;

// Gets the absolute path to the root directory
pub fn root_dir() -> String {
    let mut directory_found = false;
    let current_dir = env::current_dir().unwrap();
    let mut path = current_dir.to_str().unwrap();

    if let Ok(entries) = fs::read_dir(&path) {
        for entry in entries {
            if let Ok(entry) = entry {
                let file: String = entry.file_name().into_string().unwrap();
                if file == "anemone.toml".to_string() {
                    // println!("Found: {}", &file);
                    directory_found = true;
                }
            }
        }
    }

    while directory_found == false {
        let os = env::consts::OS;
        let slash = if os == "windows" { "\\" } else { "/" };

        let end_bytes = path.rfind(slash).expect("Failed to find anemone.toml");
        path = &path[0..end_bytes];

        if directory_found == false {
            if let Ok(entries) = fs::read_dir(&path) {
                for entry in entries {
                    if let Ok(entry) = entry {
                        let file: String = entry.file_name().into_string().unwrap();
                        if file == "anemone.toml".to_string() {
                            println!("Found config file");
                            directory_found = true;
                            break;
                        }
                    }
                }
            }
        } else {
            break;
        }
    }

    return path.to_string().replace("\\", "/");
}