use std::{fs::{File, self}, io::Read};
use filetime::FileTime;
use std::io::{BufReader,BufRead};

pub trait FileExt {
    fn to_string(&mut self) -> String;
}

impl FileExt for File {
    fn to_string(&mut self) -> String {
        let mut string = String::new();
        self.read_to_string(&mut string).unwrap();
        return string;
    }
}

pub fn file_date_walking(path: &String, folder: &String) -> Vec<i64> {

    let mut dates_vector = Vec::new();
    for entry in std::fs::read_dir(format!("{}/{}", path, folder)).unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();
        dates_vector.push(FileTime::from_last_modification_time(&fs::metadata(path).unwrap()).unix_seconds());
    }
    return dates_vector;
}

pub fn file_content(path: &String) -> Vec<String> {

    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let mut file_content = Vec::new();
    for line in reader.lines() {
        let line = line.unwrap();
        file_content.push(line);
    }
    return file_content;
}

pub fn folder_content(path: &String, extension: &str) -> Vec<String> {

    let mut files_vector: Vec<String> = Vec::new();

    if let Ok(entries) = fs::read_dir(path) {
        for entry in entries {
            if let Ok(entry) = entry {
                let file: String = entry.file_name().into_string().unwrap();
                if file.contains(extension){
                    files_vector.push(file);
                }
            }
        }
    }
    return files_vector;
}
