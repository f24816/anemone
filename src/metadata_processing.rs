use std::collections::HashMap;

use crate::builder::Config;
use crate::{templating, file_ops};

const TITLE_ID: u8 = 0;
const TAGS_ID: u8 = 1;
const IMG_ID: u8 = 2;

pub fn main(filename : &str, output_filename : &str, path: &String, context: &HashMap<String, String>) -> Option<HashMap<u8, String>> {
    let mut metadata_map: HashMap<u8, String> = HashMap::new();
    let mut file_content = file_ops::file_content(&filename.to_owned());
    let mut metadata_found: Vec<usize> = Vec::new(); // holds the metadata that he has found

    // For the tags we did found
    for (index, line) in file_content.iter().enumerate() {
        if line.starts_with("/draft") {
            // stop the main function with no output
            return None;
        }
        if line.starts_with("# ") {
            let value = line.replace("# ", "").trim().to_string();
            metadata_map.insert(TITLE_ID, value);
        }
        if line.starts_with("/tags:") {
            let value = line.replace("/tags:", "").trim().to_string();
            metadata_map.insert(TAGS_ID, value);
            metadata_found.push(index);
        }
        if line.starts_with("/image:") {
            let value = line.replace("/image:", "").trim().to_string();
            metadata_map.insert(IMG_ID, value);
            metadata_found.push(index);
        }
    }

    // Remove the metadata from the final version.
    let mut counter = 0;
    for index in metadata_found {
        file_content.remove(index - counter);
        counter += 1;
    }

    // Compile the markdown
    let template = file_ops::file_content(&format!("{}/{}", path, "theme/pages/article.html")).join("\n");
    std::fs::write(&output_filename, templating::article(template, file_content.join("\n"), context.clone(), &metadata_map, &get_toml_config(path))).expect("Unable to write file");
    return Some(metadata_map);
}

pub fn get_toml_config(path: &String) -> Config {
    let config_path = format!("{}/{}", &path, "anemone.toml" );
    let contents = std::fs::read_to_string(config_path)
        .expect("Something went wrong reading the file");
    let config: Config = toml::from_str(&contents)
        .expect("Failed to parse TOML data");
    return config;
}
