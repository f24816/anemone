use std::fs;
use async_std::task;
use colorful::{Colorful, RGB};

mod metadata_processing;
mod markdown_compiler;
mod database;
mod builder;
mod cli;
mod templating;
mod file_ops;

const VERSION: &str = env!("CARGO_PKG_VERSION");
static mut DEBUG: bool = false;

// TODO: Improve font loadtimes

fn main() {
    //----- 🐦 Hello World 🐦 -----
    let light_green: RGB = from_hexadecimal("#82D645");
    let light_blue: RGB = from_hexadecimal("#ADD8E6");
    let orange: RGB = from_hexadecimal("#F24816");

    // comand line argument called debug
    let mut dev: bool = false;
    let args: Vec<String> = std::env::args().collect();
    if args.len() > 1 && args[1] == "dev" {
        unsafe {
            DEBUG = true;
            dev = true;
        }
    }

    println!("{} {}{}",
        "Anemone".color(orange).bold(),
        "v".color(orange),
        VERSION.color(orange)
    );

    let path = cli::root_dir();
    database::init(&path).unwrap();
    println!("Building website...");
    builder::main(&path, dev);

    if dev == true {
        println!("{}{}", " 📡 Running Live Server at: ", "http://127.0.0.1:8080/".color(light_blue));
        task::block_on(async {
            tokio_j(&path);
        });
    }

    println!("{}", " ✔ Finished Task".color(light_green));
}

fn from_hexadecimal(hex: &str) -> RGB {
    let hex = hex.trim_start_matches("#");
    let r = u8::from_str_radix(&hex[0..2], 16).unwrap();
    let g = u8::from_str_radix(&hex[2..4], 16).unwrap();
    let b = u8::from_str_radix(&hex[4..6], 16).unwrap();
    RGB::new(r, g, b)
}

#[tokio::main]
async fn tokio_j(path: &str) {
    use live_server::listen;

    let main_task_handle = tokio::spawn(listen("127.0.0.1", 8080, "./.public_dev/"));
    let key_listener_handle = tokio::spawn(key_listener());

    tokio::select! {
        _ = main_task_handle => (),
        _ = key_listener_handle => {
            // You can gracefully stop your main task here
            let public_path = format!("{}/.public_dev/style.css", path);
            let theme_path = format!("{}/theme/style.css", path);
            fs::copy(public_path, theme_path).unwrap();
        }
    }
}

async fn key_listener() {
    use std::io::{stdout, stdin, Write, Read};
    let mut stdout = stdout();
    stdout.write(b"Press any key to stop the server...").unwrap();
    stdout.flush().unwrap();
    stdin().read(&mut [0]).unwrap();
}