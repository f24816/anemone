use pulldown_cmark::{Parser, Options, html, Event};

pub fn compile(input: &String) -> String {
    let mut data_html_converted = String::new();
    let mut options = Options::empty();
    options.insert(Options::ENABLE_STRIKETHROUGH);
    options.insert(Options::ENABLE_FOOTNOTES);
    options.insert(Options::ENABLE_TABLES);
    options.insert(Options::ENABLE_HEADING_ATTRIBUTES);

    let parser = Parser::new_ext(&input.as_str(), options);
    let parser = parser.map(|event| match event {
        Event::SoftBreak => Event::HardBreak,
        _ => event
    });

    html::push_html(&mut data_html_converted, parser);

    let data_html_converted_vector: Vec<&str> = data_html_converted.split("\n").collect();

    let mut output: Vec<String> = Vec::new();
    for i in 0..data_html_converted_vector.len() {
        let current_line: String = data_html_converted_vector[i].to_string();
        let converted_line: String = current_line.clone();
        output.push(converted_line);
    }
    return output.join("\n");
}