use rusqlite::{Connection, Result};

#[derive(Debug)]
struct File{
    name: String,
    path: String,
    tags: String, // tags will be JSON type
    image: String,
    date_created: String,
    date_modified: String,
}

pub fn init(path: &String) -> Result<()> {
    // get relative path
    let database_path = format!("{}/{}", path, "index.db");
    // Check if the database exists
    if !std::path::Path::new(database_path.as_str()).exists() {
        // Create the database
        let _file = std::fs::File::create(database_path).unwrap();
        let database_connection = Connection::open("index.db")?;
        database_connection.execute(
            "CREATE TABLE files (
                    id              INTEGER PRIMARY KEY,
                    name            TEXT NOT NULL,
                    path            TEXT NOT NULL,
                    tags            TEXT,
                    image           TEXT,
                    date_created    TEXT NOT NULL,
                    date_modified   TEXT NOT NULL
                    )",
            [],
        )?;
    }
    Ok(())
}

pub fn clear_database() -> Result<()> {
    let database_connection = Connection::open("index.db")?;
    database_connection.execute("DELETE FROM files;",[],)?;
    Ok(())
}

pub fn list_parameters() -> Result<()> {
    let database_connection = Connection::open("index.db")?;
    let mut stmt = database_connection.prepare("SELECT name, path, tags, date_created, date_modified FROM files")?;

    // list all values of the table
    let file_iter = stmt.query_map([], |row| {
        let name: Option<String> = row.get(0)?;
        let path: Option<String> = row.get(1)?;
        let tags: Option<String> = row.get(2)?;
        let image: Option<String> = row.get(3)?;
        let date_created: Option<String> = row.get(4)?;
        let date_modified: Option<String> = row.get(5)?;
        Ok(File {
            name: name.unwrap(),
            path: path.unwrap(),
            tags: tags.unwrap(),
            image: image.unwrap(),
            date_created: date_created.unwrap(),
            date_modified: date_modified.unwrap(),
        })
    }).expect("select failed");

    println!("Files Found:");
    for file in file_iter {
        println!("{:?}", file?);
    }
    Ok(())
}

pub fn add_file(name: String, path: String, tags: String, mut image: String, date_created: String, date_modified: String) -> Result<()> {
    if image.is_empty(){
        image = format!("\"https://placehold.co/600x400?text=(^_^)+no+image+yet\"");
    }
    let database_connection = Connection::open("index.db")?;
    database_connection.execute(
        "INSERT INTO files (name, path, tags, image, date_created, date_modified) VALUES (?1, ?2, ?3, ?4, ?5, ?6)",
        [name, path, tags, image, date_created, date_modified],
    )?;
    Ok(())
}

pub fn update_file(name: String, path: String, tags: String, mut image: String, date_modified: String) -> Result<()> {
    if image.is_empty(){
        image = format!("\"https://placehold.co/600x400?text=(^_^)+no+image+yet\"");
    }
    let database_connection = Connection::open("index.db")?;
    database_connection.execute(
        "UPDATE files SET name = ?1, tags = ?2, image= ?3, date_modified = ?4 WHERE path = ?5",
        [name, tags, image, date_modified, path],
        )?;
        Ok(())
}

pub fn remove_file(path: String) -> Result<()> {
    let database_connection = Connection::open("index.db")?;
    database_connection.execute(
        "DELETE FROM files WHERE path = ?1",
        [path],
    )?;
    Ok(())
}

// check if a file contains a cetain tag
pub fn check_tag(tag: String) -> Result<()> {
    let database_connection = Connection::open("index.db")?;
    let mut tag_stmt = database_connection.prepare("SELECT tags FROM files WHERE tags LIKE '%?1%'")?;
    let tag_iter = tag_stmt.query_map([], |row| {
        let tags: Option<String> = row.get(0)?;
        Ok(tags.unwrap())
    }).expect("select failed");

    println!("Files with tag {}:", tag);
    for file in tag_iter {
        println!("{:?}", file?);
    }
    Ok(())
}

pub fn list_all() -> Result<Vec<String>> {
    let database_connection = Connection::open("index.db")?;
    // Extract the path value of all entries
    let mut stmt = database_connection.prepare("SELECT path FROM files")?;
    let file_iter = stmt.query_map([], |row| {
        let path: Option<String> = row.get(0)?;
        Ok(path.unwrap())
        })?;
    let mut paths: Vec<String> = Vec::new();
    for (i, vector) in file_iter.enumerate() {
        if i != 0 {
            paths.push(vector?);
        }
    }
    return Ok(paths);
}

pub fn get_date(path: String) -> Result<String> {
    let database_connection = Connection::open("index.db")?;
    let mut date_stmt = database_connection.prepare("SELECT date_modified FROM files WHERE path = ?1")?;
    let date_iter = date_stmt.query_map([path], |row| {
        let date_modified: Option<String> = row.get(0)?;
            Ok(date_modified.unwrap())
    }).expect("select failed");

    for file in date_iter {
        return Ok(file?);
    }
    Ok(String::from(""))
}

pub fn get_creation_date(path: &String) -> Result<String> {
    let database_connection = Connection::open("index.db")?;
    let mut date_stmt = database_connection.prepare("SELECT date_modified FROM files WHERE path = ?1")?;
    let date_iter = date_stmt.query_map([path], |row| {
        let date_modified: Option<String> = row.get(0)?;
            Ok(date_modified.unwrap())
    }).expect("select failed");

    for file in date_iter {
        return Ok(file?);
    }
    Ok(String::from(""))
}

pub fn get_name(path: &String) -> Result<String> {
    let database_connection = Connection::open("index.db")?;
    let mut name_stmt = database_connection.prepare("SELECT name FROM files WHERE path = ?1")?;
    let name_iter = name_stmt.query_map([path], |row| {
        let name: Option<String> = row.get(0)?;
            Ok(name.unwrap())
    }).expect("select failed");

    for file in name_iter {
        return Ok(file?);
    }
    Ok(String::from(""))
}

pub fn get_image(path: &String) -> Result<String> {
    let database_connection = Connection::open("index.db")?;
    let mut name_stmt = database_connection.prepare("SELECT image FROM files WHERE path = ?1")?;
    let name_iter = name_stmt.query_map([path], |row| {
        let name: Option<String> = row.get(0)?;
            Ok(name.unwrap())
    }).expect("select failed");

    for file in name_iter {
        return Ok(file?);
    }
    Ok(String::from(""))
}
