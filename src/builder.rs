use filetime::FileTime;
use std::collections::HashMap;
use std::fs::{self, File};
use std::io::Write;
use std::path::Path;
use crate::file_ops::{*, self};
use crate::{metadata_processing, database};
use crate::templating;

const ARTICLES_PUBLIC_PATH: &str = "en/";

#[derive(serde::Deserialize)]
pub struct Config {
    pub website_name: String,
    pub website_address: String
}

pub fn main(path: &String, dev: bool) {
    // Check if we are doing a dev build
    let public_folder_path: &str;
    if dev == true {
        public_folder_path = ".public_dev";
    }
    else {
        public_folder_path = "public";
    }

    // Initialise variables
    let mut fast_compile: bool;
    let mut drafts: Vec<&String> = Vec::new();
    let context = get_components(path);

    loop {
        if directory_changed(path, "components") == true {
            fast_compile = false;
            break;
        }
        if directory_changed(path, "pages") == true {
            fast_compile = false;
            break;
        }
        if dev == true{
            fast_compile = false;
            break;
        }
        fast_compile = true;
        break;
    }

    // Create empty folders for the website
    let article_path: String = format!("{}/{public_folder_path}/{}", path, ARTICLES_PUBLIC_PATH);
    fs::create_dir_all(article_path).unwrap();

    // File walking for the articles
    let markdown_files_path: String = format!("{}/{}", path, "articles/");
    let markdown_files: Vec<String> = file_ops::folder_content(&markdown_files_path, ".md");

    for file_name in markdown_files.iter() {
        // Get the absolute paths of the source and output files
        let name = file_name.replace(".md", "");

        let mut html_path = format!("{}/{public_folder_path}/{}", path, ARTICLES_PUBLIC_PATH);
        html_path.push_str(&name);
        html_path.push_str(".html");

        let mut md_path = format!("{}/{}", path, "articles/");
        md_path.push_str(&name);
        md_path.push_str(".md");

        // Check if a html file exists
        if std::path::Path::new(html_path.as_str()).exists(){
            // The html file was generated
            if fast_compile == false {
                // Rewrite the HTML file
                match metadata_processing::main(&md_path, &html_path, path, &context) {
                    // Skip this block if it's a draft (function returns None)
                    None => {drafts.push(file_name)}
                    Some(metadata) => {
                        // Compile the Markdown file & extract the metadata
                        let name = metadata.get(&0).unwrap_or(&String::from("")).to_owned();
                        let tags = metadata.get(&1).unwrap_or(&String::from("")).to_owned();
                        let image = metadata.get(&2).unwrap_or(&String::from("")).to_owned();
                        let file_time = FileTime::from_last_modification_time(&fs::metadata(&md_path).unwrap()).unix_seconds();
                        // Update the database
                        database::update_file(name, file_name.clone(), tags, image, file_time.to_string()).unwrap();
                    }
                }
            }
            else {
                // Check if the markdown file was modified
                let file_time: String = FileTime::from_last_modification_time(&fs::metadata(&md_path).unwrap()).unix_seconds().to_string();
                let stored_time: String = database::get_date(file_name.clone()).unwrap_or(String::from(""));
                if file_time != stored_time {
                    // Rewrite the HTML file
                    println!("Rewriting the HTML file");
                    match metadata_processing::main(&md_path, &html_path, path, &context) {
                        // Skip this block if it's a draft (function returns None)
                        None => {drafts.push(file_name)}
                        Some(metadata) => {
                            // Compile the Markdown file & extract the metadata
                            let name = metadata.get(&0).unwrap_or(&String::from("")).to_owned();
                            let tags = metadata.get(&1).unwrap_or(&String::from("")).to_owned();
                            let image = metadata.get(&2).unwrap_or(&String::from("")).to_owned();
                            let file_time = FileTime::from_last_modification_time(&fs::metadata(&md_path).unwrap()).unix_seconds();
                            // Update the database
                            database::update_file(name, file_name.clone(), tags, image, file_time.to_string()).unwrap();
                        }
                    }
                }
            }
        } else {
            // The html file doesen't exist (new article)
            fast_compile = false;
            match metadata_processing::main(&md_path, &html_path, path, &context) {
                // Skip this block if it's a draft (returns None)
                None => {drafts.push(file_name)}
                // Compile the Markdown file & extract the metadata
                Some(metadata) => {
                    println!("Creating new article: {}", &file_name);
                    let name = metadata.get(&0).unwrap_or(&String::from("")).to_owned();
                    let tags = metadata.get(&1).unwrap_or(&String::from("")).to_owned();
                    let image = metadata.get(&2).unwrap_or(&String::from("")).to_owned();
                    let file_time = FileTime::from_last_modification_time(&fs::metadata(&md_path).unwrap()).unix_seconds();
                    database::add_file(name, file_name.clone(), tags, image, file_time.to_string(), file_time.to_string()).unwrap();
                }
            }
        }
    }

    // Getting the vector of filepaths in the database
    let db_filepaths = database::list_all().unwrap();
    // Filtering the vector of markdown files to only those that still exist in the database.
    for databse_item in db_filepaths.iter() {
        if markdown_files.contains(&databse_item) == false {
            // Remove the file from the database & filesystem
            fast_compile = false;
            println!("File deleted: {}", &databse_item);
            database::remove_file(databse_item.clone()).unwrap();
            let name = databse_item.replace(".md", ".html");
            let mut html_path = format!("{}/{public_folder_path}/{}", path, ARTICLES_PUBLIC_PATH);
            html_path.push_str(&name);
            fs::remove_file(html_path).unwrap_or_else(|_| ()); // BUG: This might fail.
        } else {
        }
    }

    // Compare html file in the public and articles directory
    let mut html_files: Vec<String> = Vec::new();
    let html_files_path = format!("{}/{public_folder_path}/{}", path, ARTICLES_PUBLIC_PATH);
    if let Ok(entries) = fs::read_dir(html_files_path) {
        for entry in entries {
            if let Ok(entry) = entry {
                let file: String = entry.file_name().into_string().unwrap();
                if file.contains(".html"){
                    html_files.push(file);
    }}}}

    for html_file in html_files.iter() {
        let name = html_file.replace(".html", ".md");
        if markdown_files.contains(&name) == false {
            let mut html_path2 = format!("{}/{public_folder_path}/{}", path, ARTICLES_PUBLIC_PATH);
            html_path2.push_str(&html_file);
            println!("File we deleted {} ", &html_path2);
            fs::remove_file(html_path2).unwrap();
        } else {
    }}

    // Copy the optimised stylesheet to the public directory
    use lightningcss::stylesheet::{StyleSheet, ParserOptions, MinifyOptions, PrinterOptions};
    let style_path = format!("{}/{}", &path, "theme/style.css");
    let absolute_public_path = format!("{}/{public_folder_path}/{}", &path, "style.css");

    let contents = fs::read_to_string(style_path)
        .expect("Something went wrong reading the file");

    let mut stylesheet = StyleSheet::parse(&contents, ParserOptions::default()).unwrap();
    stylesheet.minify(MinifyOptions::default()).unwrap();
    let minified_css = stylesheet.to_css(PrinterOptions::default()).unwrap();
    std::fs::write(absolute_public_path, minified_css.code).unwrap();

    // Copy the favicon.ico to the public directory
    let favicon_path = format!("{}/{}", &path, "theme/favicon.ico");
    let public_favicon_path = format!("{}/{public_folder_path}/{}", &path, "favicon.ico");
    fs::copy(favicon_path, public_favicon_path).unwrap();

    // Trigger a complete rebuild if necessary
    if fast_compile == false {
        complete_rebuild(public_folder_path, path, html_files, context);
    }
}

fn complete_rebuild(public_folder_path: &str, path: &String, html_files: Vec<String>, mut context: HashMap<String, String>) {
    // Add the short list to the index.html
    let mut long_list: Vec<String> = Vec::new();
    for file in html_files.iter() {
        let md_name = file.replace(".html", ".md");
        let title = database::get_name(&md_name).unwrap();
        let image = database::get_image(&md_name).unwrap();
        let raw_date: i64 = database::get_creation_date(&md_name).unwrap_or("1".to_string()).parse().unwrap_or(0);
        use chrono::prelude::*;
        let dt = Utc.timestamp_opt(raw_date, 0).unwrap();
        let date = dt.format("%B %e, %Y").to_string();
        let html_path = format!("
        <li>
        <img src={image} />
        <p class=\"date\">{date}</p>
        <p><a href=\"./{}{}\">{}</a></p>
        </li>
        ", ARTICLES_PUBLIC_PATH, file, title);
        long_list.push(html_path);
    }

    // TODO Make a short list and long list
    // long_list.iter().take(10).map(|x| x.to_string()).collect().join("\n");

    let config = metadata_processing::get_toml_config(path);
    context.insert("longList".to_string(), format!("<ul>\n{}\n</ul>", long_list.join("\n")));

    // Copy index.html to the public directory
    let index_file_content: Vec<String> = file_content(&format!("{}/{}", &path, "theme/pages/index.html"));
    let index_file_path = format!("{public_folder_path}/{}", "index.html");
    std::fs::write(index_file_path,
        templating::index(index_file_content.join("\n"), &context, &config)
    ).unwrap();

    // Copy the pages folder to the public directory
    for entry in fs::read_dir(format!("{}/{}", &path, "theme/pages")).unwrap() {
        let entry = entry.unwrap();
        let file_name = entry.file_name().into_string().unwrap();
        if matches!(file_name.as_str(), "index.html" | "404.html" | "article.html") {
            continue;
        }
        let page_file_content: Vec<String> = file_content(&format!("{}/{}/{}", &path, "theme/pages", file_name));
        std::fs::write(format!("{public_folder_path}/{}", file_name), templating::page(page_file_content.join("\n"), &context, &config)).unwrap();
    }
}

fn directory_changed(path: &String, directory: &str) -> bool {
    // Compare dates of the files in the directory with the ones in the cache file.
    let dates_vector: Vec<i64> = file_date_walking(&path, &format!("theme/{}", directory));
    let dates_string: String = dates_vector.iter().map(|x| x.to_string()).collect::<Vec<String>>().join(",");

    let cache_file_contents: Result<File, bool> = match fs::File::open(format!("{}/.cache/{}", &path, directory)) {
        Ok(file) => Ok(file),
        Err(_) => {
            // Create cache file if it doesn't exist.
            if !Path::new(&format!("{}/.cache", &path)).exists() {
                fs::create_dir(format!("{}/.cache", &path)).unwrap();
            }
            let mut cache_file = fs::File::create(format!("{}/.cache/{}", &path, directory)).unwrap();
            cache_file.write_all(dates_string.as_bytes()).unwrap();
            return true; // Trigger full rebuild
        }
    };

    let cache_file_string: String = match cache_file_contents {
        Ok(mut file) => file.to_string(),
        Err(_) => "".to_string(),
    };

    if cache_file_string != dates_string {
        // Replace cache file with new dates.
        let mut cache_file = fs::File::create(format!("{}/.cache/{}", &path, directory)).unwrap();
        cache_file.write_all(dates_string.as_bytes()).unwrap();
        return true; // Trigger full rebuild
    }

    return false; // The directory hasn't changed
}

fn get_components(path: &String) -> HashMap<String, String> {
    use inflector::Inflector;
    let mut context: HashMap<String, String> = HashMap::new();

    // Itterate over the components directory
    for entry in fs::read_dir(format!("{}/{}", &path, "theme/components")).unwrap() {
        let entry = entry.unwrap();
        let file_name = entry.file_name().into_string().unwrap();
        let file_path = format!("{}/theme/components/{}", &path, file_name);
        let file_content = file_ops::file_content(&file_path);
        let file_content = file_content.join("\n");
        let file_short = file_name.replace(".html", "");
        context.insert(file_short.to_camel_case(), file_content);
    }
    return context;
}
