use std::collections::HashMap;
use handlebars::Handlebars;

use crate::builder::Config;
use crate::markdown_compiler::compile;

pub fn article(template: String, input: String, mut context: HashMap<String, String>, metadata_map: &HashMap<u8, String>,  config: &Config) -> String {
    let image = format!("<img src={} />\n", metadata_map.get(&2).unwrap_or(&String::from("\"https://placehold.co/600x400?text=(^_^)+no+image+yet\"")).to_string());
    context.insert("content".to_string(), format!("{}<div class=\"content\">{}</div>", image, compile(&input)));
    let mut handlebars = Handlebars::new();
    handlebars
        .register_template_string("article", template)
        .unwrap();

    let output = base_template(handlebars.render("article", &context).unwrap(), config, Ok(metadata_map));
    return output;
}

pub fn index(template: String, context: &HashMap<String, String>, config: &Config) -> String {
    let mut handlebars = Handlebars::new();
    handlebars
        .register_template_string("index", &template)
        .unwrap();
    let output = base_template(handlebars.render("index", &context).unwrap(), config, Err(()));
    return output;
}

pub fn page(template: String, context: &HashMap<String, String>, config: &Config) -> String {

    fn check_for_markdown(template: String) -> String {
        if template.contains("<markdown>") {
            // find string between <markdown> and </markdown>
            let start_bytes = template.find("<markdown>").unwrap_or(0);
            let end_bytes = template.find("</markdown>").unwrap_or(template.len());
            let markdown = &template[start_bytes..end_bytes].replace("<markdown>", "");
            // compile markdown
            let compiled_markdown = compile(markdown);
            // replace string with compiled markdown
            let template = template.replace("<markdown>", "").replace("</markdown>", "").replace(markdown, &compiled_markdown);
            return template;
        }
        else {
            return template;
        }
    }

    let template = check_for_markdown(template);

    let mut handlebars = Handlebars::new();
    handlebars
        .register_template_string("page", &template)
        .unwrap();
    let output = base_template(handlebars.render("page", context).unwrap(), config, Err(()));
    return output;
}

fn base_template(body: String, config: &Config, metadata_map: Result<&HashMap<u8, String>, ()>) -> String {
    let css_location: String;
    unsafe {
        if crate::DEBUG == false {
            css_location = format!("{}/style.css", config.website_address);
        }
        else {
            css_location = format!("{}/style.css", "http://127.0.0.1:8080");
        }
    }
    let open_graph: Vec<String> = match metadata_map {
        Ok(metadata_map) => {
            // create a string with all the open graph tags
            let mut open_graph: Vec<String> = Vec::new();
            let title = metadata_map.get(&0).unwrap();
            let image = metadata_map.get(&2).unwrap();
            open_graph.push(format!("<meta property=\"og:title\" content=\"{}\"/>", title));
            open_graph.push(format!("<meta property=\"og:image\" content={}/>", image));
            open_graph
        },
        Err(_) => Vec::new()
    };
    let literal = format!(
r#"<!DOCTYPE html>
<html>
<head>
<title>{}</title>
<link rel="icon" href="/favicon.ico">
<meta charset="utf-8">
{}
<link rel="stylesheet" href="{}" >
</head>
<body>
{}
</body>
</html>"# ,config.website_name, open_graph.join("\n"), css_location, body);
    return literal;
}