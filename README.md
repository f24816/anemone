# Anemone
**Anemone** is a Website Builder focused on ease of use.

It helps you build *blogs* and *wiki*-like websites that are easy to setup and use and focuses on giving you a more simple and straightforward approach to setting up a website.

It has been built to be a self-hosted replacement of publishing platforms like WordPress and Medium by allowing users to have full ownership and control of their content without relying on a third-party service.

### Our goals are:

🧠 **Being Intuitive**</br>
Everything you write is plain text using `markdown` , `html` and `CSS` files.
It also defaults to "hard breaks" for consistency.

🥱 **Automating the boring stuff**</br>
Anemone gives you a working site that you build upon and change it to your hearts content.

✒️ **Being easy to write for**</br>
There is no need for `yaml` frontmatter. It will accept any plain markdown file.

